<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<c:if test="${activeLink == null}">
	<c:set var="activeLink" value="home" />
</c:if>

<div id="header_top">
	<img src="${pageContext.request.contextPath}/resources/images/logo.png" width="100" height="75" alt="" />
	<div id="header_links">
		<ul>
			<li><a
				class="<c:if test="${activeLink == 'home'}">active</c:if>"
				href="${pageContext.request.contextPath}/home">Основна</a></li>
			<li><a
				class="<c:if test="${activeLink == 'ebooks'}">active</c:if>"
				href="${pageContext.request.contextPath}/ebook/list">Книги</a></li>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li><a
					class="<c:if test="${activeLink == 'authors'}">active</c:if>"
					href="${pageContext.request.contextPath}/admin/author/list">Автори</a></li>
				<li><a
					class="<c:if test="${activeLink == 'publishers'}">active</c:if>"
					href="${pageContext.request.contextPath}/admin/publisher/list">Издавачи</a></li>
				<li><a
					class="<c:if test="${activeLink == 'categories'}">active</c:if>"
					href="${pageContext.request.contextPath}/admin/category/list">Категории</a></li>
			</sec:authorize>

			<li><a
				class="<c:if test="${activeLink == 'users'}">active</c:if>"
				href="${pageContext.request.contextPath}/admin/user/list">Корисници</a></li>
			<sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
				<s:url var="logout" value="/j_spring_security_logout" />
				<li><a
					class="<c:if test="${activeLink == 'logout' }">active</c:if>"
					href="${logout}"> Одјави се</a></li>
			</sec:authorize>
		</ul>
	</div>
</div>

<div id="header_bottom">
	<div id="slogan">
		<p>Книгата е најдобриот другар!</p>
	</div>
</div>
