package my.ebooks.service;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T> {

	public T find(Serializable id);

	public List<T> list();

	public void saveOrUpdate(T t);

	public void delete(T t);

}