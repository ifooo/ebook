<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="right">
    <form method="post" action="SearchController">
        <p><input type="text" name="search_text" placeholder="Барај..." />
        <select id="search_criteria" name="search_criteria">
	       <option value="title">Наслов</option>
	       <option value="author">Автор</option>
	       <option value="publisher">Издавач</option>
	       <option value="category">Категорија</option>
	       <option value="ISBN">ISBN</option>
        </select> <input type="submit" value="" id="search_submit" /></p>
    </form>

</div>
