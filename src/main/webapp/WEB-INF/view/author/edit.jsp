<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Автор</h1>
				    <br />
                    
                    <sf:form modelAttribute="author" method="post">
                    
                    	<c:if test="${action == 'update'}">                    		
                            <sf:input type="hidden" path="id" value="${author.id}" />
                            <sf:input type="hidden" path="optlock" value="${author.optlock}" />
                    	</c:if>
                    	
                        <p>                            
                 			Име <br />
                           	<c:if test="${action == 'new'}">
                           		<sf:input type="text" path="firstName" value="" />    
                           	</c:if>
                           	<c:if test="${action == 'update'}">
                            	<sf:input type="text" path="firstName" value="${author.firstName}" />    
                           	</c:if> 
                        </p>
                                           
                        <p>
                           Презиме <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="lastName" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="lastName" value="${author.lastName}" />    
                            </c:if>
                        </p>
                                                                        
                    	<p>
                            <c:choose>
                                <c:when test="${action == 'new'}"> 
                                	<input type="submit" id="as_link" value="Внеси" />
                                </c:when>
                                <c:when test="${action == 'update'}">
                                    <input type="submit" id="as_link" value="Ажурирај" />
                                </c:when>
                            </c:choose>
                        
                        &nbsp; &nbsp;
                        <a class="link" href="${pageContext.request.contextPath}/admin/author/list">Автори</a>
                        </p>
                    
                    </sf:form>
                    
                    
                    <br />

				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>