package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.BookDAO;
import my.ebooks.model.Book;
import my.ebooks.service.BookService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("bookService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BookServiceImpl implements BookService {
	@Autowired
	private BookDAO bookDAO;

	@Override
	public Book find(Serializable id) {
		if (id == null) {
			return null;
		}
		return bookDAO.find(id);
	}

	@Override
	public List<Book> list() {
		return bookDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(Book book) {
		if (book != null) {
			bookDAO.saveOrUpdate(book);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Book book) {
		if (book != null) {
			bookDAO.delete(book);
		}
	}

}
