<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Книгa</h1>
				    <br />
                    <img src="${pageContext.request.contextPath}Repo/images/${book.id}.jpg" width="150" height="250"/><br />
                    ISBN: <strong>${book.isbn}</strong> <br /> 
                    Наслов: <strong>${book.title}</strong> <br />
                    <!--  Автор: <strong>${book.authors}</strong> <br /> -->
                    <!--  Категорија: <strong>${book.categories}</strong> <br /> -->
                    Година на издавање: <strong>${book.copyrightYear}</strong> <br />
                    Издавач: <strong>${book.publisher.name}</strong> <br />
                    Број на страни: <strong>${book.numberOfPages}</strong> <br />
                    Цена: <strong>${book.price} денари</strong> <br />
                    <sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')">
                    <a href="${pageContext.request.contextPath}Repo/ebooks/${book.id}.pdf">Превземи</a><br />
                    </sec:authorize>
                    <br />

                    <c:if test="${confirm_delete != true}">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a class="link" href="${pageContext.request.contextPath}/ebook/update/${book.id}">Ажурирај</a>
                        &nbsp; &nbsp; 
                        <a class="link" href="${pageContext.request.contextPath}/ebook/delete/${book.id}">Избриши</a>
                        </sec:authorize>    
                        &nbsp; &nbsp; 
                        <a class="link" href="${pageContext.request.contextPath}/ebook/list">Книги</a>
                    </c:if>
                                        
				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>