package my.ebooks.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "books")
public class Book extends AbstractModel {
	private static final long serialVersionUID = 1L;

	@Column(name = "isbn", nullable = false, unique = true, length = 20)
	private String isbn;

	@Column(name = "title", nullable = false)
	private String title;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Author> authors;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Category> categories;

	@Column(name = "copyright_year")
	private Integer copyrightYear;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Publisher publisher;

	@Column(name = "edition_number")
	private Integer editionNumber = 1;

	@Column(name = "cover_image")
	private String coverImage;

	@Column(name = "pages")
	private Integer numberOfPages;

	@Column(name = "book_file")
	private String bookFileName;

	@Column(name = "price")
	private BigDecimal price;

	public Book() {
		super();
	}

	public Book(String isbn, String title, Set<Author> authors,
			Set<Category> categories, Integer copyrightYear, Publisher publisher,
			Integer editionNumber, String coverImage, Integer numberOfPages,
			String bookFileName, BigDecimal price) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.authors = authors;
		this.categories = categories;
		this.copyrightYear = copyrightYear;
		this.publisher=publisher;
		this.editionNumber = editionNumber;
		this.coverImage = coverImage;
		this.numberOfPages = numberOfPages;
		this.bookFileName = bookFileName;
		this.price = price;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Integer getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(Integer copyrightYear) {
		this.copyrightYear = copyrightYear;
	}
	public void setPublisher(Publisher publisher){
		this.publisher=publisher;
	}
	
	public Publisher getPublisher(){
		return publisher;
	}

	public Integer getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(Integer editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public String getBookFileName() {
		return bookFileName;
	}

	public void setBookFileName(String bookFileName) {
		this.bookFileName = bookFileName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", this.id)
				.append("isbn", this.isbn).append("title", this.title)
				.append("authors", this.authors)
				.append("categories", this.categories)
				.append("copyrightYear", this.copyrightYear)
				.append("editionNumber", this.editionNumber)
				.append("coverImage", this.coverImage)
				.append("numberOfPages", this.numberOfPages)
				.append("bookFileName", this.bookFileName)
				.append("price", this.price).append("enabled", this.enabled)
				.toString();
	}

}
