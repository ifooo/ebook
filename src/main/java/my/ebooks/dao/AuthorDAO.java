package my.ebooks.dao;

import my.ebooks.model.Author;

public interface AuthorDAO extends GenericDAO<Author> {

}
