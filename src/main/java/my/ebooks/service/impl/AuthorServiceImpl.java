package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.AuthorDAO;
import my.ebooks.model.Author;
import my.ebooks.service.AuthorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("authorService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuthorServiceImpl implements AuthorService {
	@Autowired
	private AuthorDAO authorDAO;

	@Override
	public Author find(Serializable id) {
		if (id == null) {
			return null;
		}
		return authorDAO.find(id);
	}

	@Override
	public List<Author> list() {
		return authorDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(Author author) {
		if (author != null) {
			authorDAO.saveOrUpdate(author);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Author author) {
		if (author != null) {
			authorDAO.delete(author);
		}
	}

}
