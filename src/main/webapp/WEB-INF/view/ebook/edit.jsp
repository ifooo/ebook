<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Книга</h1>
				    <br />
                    <sf:form modelAttribute="book" method="post" enctype="multipart/form-data">
                    
                    	<c:if test="${action == 'update'}">                    		
                            <sf:input type="hidden" path="id" value="${book.id}" />
                            <sf:input type="hidden" path="optlock" value="${book.optlock}" />
                    	</c:if>
                    	
                        <p>                            
                 			ISBN <br />
                           	<c:if test="${action == 'new'}">
                           		<sf:input type="text" path="isbn" value="" />    
                           	</c:if>
                           	<c:if test="${action == 'update'}">
                            	<sf:input type="text" path="isbn" value="${book.isbn}" />    
                           	</c:if> 
                        </p>
                                           
                        <p>
                           Наслов <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="title" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="title" value="${book.title}" />    
                            </c:if>
                        </p>
                        
                       <p>
                           	Автор <br />
                          	<sf:select path="authors" multiple="multiple">
   								<sf:option value="-1" label="--- Избери Автор ---"/>
   								<sf:options itemValue="id" itemLabel="firstName" items="${authorList}" />
							</sf:select>
                        </p>
                        
                        <p>
                           	Категорија <br />
                          	<sf:select path="categories" multiple="multiple">
   								<sf:option value="-1" label="--- Избери Категорија ---"/>
   								<sf:options itemValue="id" itemLabel="name" items="${categoryList}" />
							</sf:select>
                        </p>
                        
                        <p>
                           	Издавачи <br />
                          	<sf:select path="publisher" multiple="multiple">
   								<sf:option value="-1" label="--- Избери Издавач ---"/>
   								<sf:options itemValue="id" itemLabel="name" items="${publisherList}" />
							</sf:select>
                        </p>
                        
                        
                        <p>
                           Година на издавање <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="copyrightYear" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="copyrightYear" value="${book.copyrightYear}" />    
                            </c:if>
                        </p>
                        
                        <p>
                           Едиција на книга <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="EditionNumber" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="copyrightYear" value="${book.editionNumber}" />    
                            </c:if>
                        </p>
                        
                        <p>
                           Број на страни <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="numberOfPages" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="numberOfPages" value="${book.numberOfPages}" />    
                            </c:if>
                        </p>
                        
                        <p>
                           Цена <br />
                           <c:if test="${action == 'new'}">
                            	<sf:input type="text" path="price" value="" />    
                            </c:if>
                            <c:if test="${action == 'update'}">
                            	<sf:input type="text" path="price" value="${book.price}" />    
                            </c:if>
                        </p>
                        
                        <p>
                        
                        	
                        	<tr>
                        		<th><label for="coverImage">Корица</label></th>
                        		<td><input name="coverImage" type="file" /></td>
                        	</tr>
                      
                        </p>
                        
                      <p>
                        Внеси книга<br />
                        
                        		<td><input name="bookFileName" type="file"/></td>
                        	
                        
                       
                        </p>                                      
                    	
                    		<c:choose>
                                <c:when test="${action == 'new'}"> 
                                	<input type="submit" id="as_link" value="Внеси" />
                                </c:when>
                                <c:when test="${action == 'update'}">
                                    <input type="submit" id="as_link" value="Ажурирај" />
                                </c:when>
                            </c:choose> 
                        
                       
                    	 &nbsp; &nbsp;
                        <s:url var="ebooklist" value="/ebook/list" />
                        <a class="link" href="${ebooklist}">Книги</a>
                        
                    
                    </sf:form>
                    
                    
                    
                    <br />

				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>