package my.ebooks.service;

import java.util.List;

import my.ebooks.model.Book;
import my.ebooks.model.Category;

public interface CategoryService extends GenericService<Category> {
	public List<Book> findAllBooksByCategory(Category category);
}
