<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="search.jsp"%>
			
			<div id="left">
				<div class="book_wrapper">
                    <img src="${pageContext.request.contextPath}/../e-books/Spring in Action@Craig Walls@Manning Publications Co.@2011@ISBN 9781935182351@3.png" class="book_cover"/>
                   
                    <h2><a href="#">Spring in Action</a></h2>
                    <p>Here is some text. Lorem ipsum dolor sit amet, Here is some text. Lorem ipsum dolor sit amet, 
                    consectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. Mauris lacus. 
                    Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla facilisi. nsectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. 
                    Mauris lacus. Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla faciFusce condimentum varius metus. Mauris lacus. Vivamus egestas facilisis eros. Here is some text. 
                    Lorem ipsum dolor sit amet, Here is some text. Lorem ipsum dolor sit amet, 
                    consectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. Mauris lacus. 
                    Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla facilisi. nsectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. 
                    Mauris lacus. Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla faciFusce condimentum varius metus. Mauris lacus. Vivamus egestas facilisis eros.</p>
                    
                    <a href="#"><fmt:message key="download" /></a>
                    
                </div>
                
				<div class="book_wrapper">
				    <img src="${pageContext.request.contextPath}/../e-books/book.png" class="book_cover"/>
				    <h2><a href="#">Bring your concepts and ideas to life!</a></h2>
			
				    <p>Here is some text. Lorem ipsum dolor sit amet,</p><p>Here is some text. Lorem ipsum dolor sit amet, 
                    consectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. Mauris lacus. 
                    Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla facilisi. nsectetuer adipiscing elit. Nulla facilisi. Fusce condimentum varius metus. 
                    Mauris lacus. Vivamus egestas facilisis eros.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Nulla faciFusce condimentum varius metus. Mauris lacus. Vivamus egestas facilisis eros.</p>
                    
                    <a href="#"><fmt:message key="download" /></a>
				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="footer.jsp"%>
	</div>
	
</body>
</html>