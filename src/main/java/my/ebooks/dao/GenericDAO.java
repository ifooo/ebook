package my.ebooks.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T> {
	public T find(Serializable id);

	public List<T> list();

	public void saveOrUpdate(T t);

	public void delete(T t);
}
