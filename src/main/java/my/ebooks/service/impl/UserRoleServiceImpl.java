package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.UserRoleDAO;
import my.ebooks.model.UserRole;
import my.ebooks.service.UserRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userRoleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserRoleServiceImpl implements UserRoleService {
	@Autowired
	private UserRoleDAO userRoleDAO;

	@Override
	public UserRole find(Serializable id) {
		if (id == null) {
			return null;
		}
		return userRoleDAO.find(id);
	}

	@Override
	public List<UserRole> list() {
		return userRoleDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(UserRole userRole) {
		if (userRole != null) {
			userRoleDAO.saveOrUpdate(userRole);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(UserRole userRole) {
		if (userRole != null) {
			userRoleDAO.delete(userRole);
		}
	}

}
