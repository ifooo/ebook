package my.ebooks.dao.impl;

import my.ebooks.dao.AuthorDAO;
import my.ebooks.model.Author;

import org.springframework.stereotype.Repository;

@Repository("authorDAO")
public class AuthorDAOImpl extends GenericDAOImpl<Author> implements AuthorDAO {

}
