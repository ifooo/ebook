package my.ebooks.dao;

import my.ebooks.model.Publisher;

public interface PublisherDAO extends GenericDAO<Publisher> {

}
