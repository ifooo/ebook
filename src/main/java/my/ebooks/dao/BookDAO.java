package my.ebooks.dao;

import my.ebooks.model.Book;

public interface BookDAO extends GenericDAO<Book> {

}
