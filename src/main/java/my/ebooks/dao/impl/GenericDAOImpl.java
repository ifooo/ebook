package my.ebooks.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import my.ebooks.dao.GenericDAO;

import org.hibernate.SessionFactory;

public abstract class GenericDAOImpl<T> implements GenericDAO<T> {

	@Resource
	protected SessionFactory sessionFactory;

	private Class<T> persistentClassType;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() {
		super();
		this.persistentClassType = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	public T find(Serializable id) {
		return (T) sessionFactory.getCurrentSession().get(persistentClassType, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list() {
		return (List<T>) sessionFactory.getCurrentSession()
				.createQuery("from " + persistentClassType.getName()).list();
	}

	@Override
	public void saveOrUpdate(T t) {
		sessionFactory.getCurrentSession().saveOrUpdate(t);
	}
	
	@Override
	public void delete(T t) {
		sessionFactory.getCurrentSession().delete(t);
	}

}
