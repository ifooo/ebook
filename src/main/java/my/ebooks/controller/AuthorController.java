package my.ebooks.controller;

import javax.validation.Valid;

import my.ebooks.model.Author;
import my.ebooks.service.AuthorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/author")
public class AuthorController {

    private final static String PAGE_VIEW = "author";
    private final static String ACTIVE_LINK = "authors";

    @Autowired
    private AuthorService authorService;
       

    /*
     * LIST
     */
    @RequestMapping("list")
    public String list(Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
        model.addAttribute(authorService.list());
        return PAGE_VIEW + "/list";
    }
    
    /*
     * NEW
     */
    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String showNewForm(Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
    	model.addAttribute("action", "new");
        model.addAttribute(new Author());
        return PAGE_VIEW + "/edit";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String save(@Valid Author author, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return PAGE_VIEW + "/edit";
        }
        authorService.saveOrUpdate(author);
        return "redirect:/admin/" + PAGE_VIEW + "/" + author.getId();
    }

    
    /*
     * VIEW
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Long id, Model model) {
        Author author = authorService.find(id);
        model.addAttribute("authors", author);
        return PAGE_VIEW + "/view";
    }

    /*
     * UPDATE
     */
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable Long id, Model model) {
    	model.addAttribute("activeLink", ACTIVE_LINK);
    	model.addAttribute("action", "update");
    	model.addAttribute(authorService.find(id));
        return PAGE_VIEW + "/edit";
    }
    
    @RequestMapping(value = "update/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid Author author, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return PAGE_VIEW + "/edit";
        }

        authorService.saveOrUpdate(author);
        
        return "redirect:/admin/" + PAGE_VIEW + "/" + author.getId();
    }
    

    /*
     * DELETE
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id, Model model) {
        authorService.delete(authorService.find(id));
        model.addAttribute("activeLink", ACTIVE_LINK);
        model.addAttribute(authorService.list());
        return PAGE_VIEW + "/list";
    }


}
