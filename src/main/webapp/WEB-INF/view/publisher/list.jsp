<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Издавачи</h1>
				    <br />
                    <table class="zebra">
                        <tr>
                         	<th> </th>
                            <th>издавач</th>
                        </tr>

                        <tbody>
                            <c:set var="count" value="0" />
                            <c:forEach var="publisher" items="${publisherList}">
                                <c:set var="count" value="${count + 1}" />
                                <tr>
                                    <td>${count}</td>
                                    <td><a href="${pageContext.request.contextPath}/admin/publisher/${publisher.id}">${publisher.name}</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    
                    <br />

                    <a class="link" href="${pageContext.request.contextPath}/admin/publisher/new">Внеси</a>
                    
				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>