package my.ebooks.dao.impl;

import my.ebooks.dao.BookDAO;
import my.ebooks.model.Book;

import org.springframework.stereotype.Repository;

@Repository("bookDAO")
public class BookDAOImpl extends GenericDAOImpl<Book> implements BookDAO {

}
