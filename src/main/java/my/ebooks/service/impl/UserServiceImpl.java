package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.UserDAO;
import my.ebooks.model.User;
import my.ebooks.service.UserService;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService {
		
	@Autowired
	private UserDAO userDAO;

	@Override
	public User find(Serializable id) {
		if (id == null) {
			return null;
		}
		User user = userDAO.find(id);
		Hibernate.initialize(user.getUserRoles());
		return user;
	}

	@Override
	public List<User> list() {
		return userDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(User user) {
		if (user != null) {
			userDAO.saveOrUpdate(user);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(User user) {
		if (user != null) {
			userDAO.delete(user);
		}
	}

}
