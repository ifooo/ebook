package my.ebooks.dao.impl;

import my.ebooks.dao.PublisherDAO;
import my.ebooks.model.Publisher;

import org.springframework.stereotype.Repository;

@Repository("publisherDAO")
public class PublisherDAOImpl extends GenericDAOImpl<Publisher> implements PublisherDAO {

}
