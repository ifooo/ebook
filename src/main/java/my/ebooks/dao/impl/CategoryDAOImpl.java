package my.ebooks.dao.impl;

import java.util.List;

import my.ebooks.dao.CategoryDAO;
import my.ebooks.model.Book;
import my.ebooks.model.Category;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository("categoryDAO")
public class CategoryDAOImpl extends GenericDAOImpl<Category> implements CategoryDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findAllBooksByCategory(Category category) { 
		Query query	= sessionFactory.getCurrentSession()
		.createQuery("from Book b where b.category.id = ?");
		query.setLong(0, category.getId());
		List<Book> booksByCategory = query.list();
		return booksByCategory;
	}

}
