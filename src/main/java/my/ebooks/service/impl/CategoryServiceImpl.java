package my.ebooks.service.impl;

import java.io.Serializable;
import java.util.List;

import my.ebooks.dao.CategoryDAO;
import my.ebooks.model.Book;
import my.ebooks.model.Category;
import my.ebooks.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("categoryService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryDAO categoryDAO;

	@Override
	public Category find(Serializable id) {
		if (id == null) {
			return null;
		}
		return categoryDAO.find(id);
	}

	@Override
	public List<Category> list() {
		return categoryDAO.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(Category category) {
		if (category != null) {
			categoryDAO.saveOrUpdate(category);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Category category) {
		if (category != null) {
			categoryDAO.delete(category);
		}
	}

	@Override
	public List<Book> findAllBooksByCategory(Category category) {
		if (category == null || category.getId() == null) {
			return null;
		}
		return categoryDAO.findAllBooksByCategory(category);
	}

}
