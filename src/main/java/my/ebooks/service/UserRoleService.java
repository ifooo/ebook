package my.ebooks.service;

import my.ebooks.model.UserRole;

public interface UserRoleService extends GenericService<UserRole> {

}
