<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- meta -->
<%@include file="../meta.jsp"%>

<body>
	<div id="wrapper">
        <!-- header -->
        <%@include file="../header.jsp"%>
		
		<div id="main_container">
		
            <!-- search -->
            <%@include file="../search.jsp"%>
			
			<div id="left">
				<div>
				
				    <h1>Издавач</h1>
				    <br />
                    
                    Име: <strong>${publisher.name}</strong> <br /> 
                   
                    
                    <br />

                    <c:if test="${confirm_delete != true}">
                        <a class="link" href="${pageContext.request.contextPath}/admin/publisher/update/${publisher.id}">Ажурирај</a>
                        &nbsp; &nbsp; 
                        <a class="link" href="${pageContext.request.contextPath}/admin/publisher/delete/${publisher.id}">Избриши</a>
                            
                        &nbsp; &nbsp; 
                        <a class="link" href="${pageContext.request.contextPath}/admin/publisher/list">Издавачи</a>
                    </c:if>
                                        
				</div>
				
				
			</div>	
		</div>
		
        <!-- footer -->
        <%@include file="../footer.jsp"%>
	</div>
	
</body>
</html>